package com.codehub.academy.springbootteam1.enums;

public enum  RepairTypeEnum {

    DEFAULT("Undefined"),
    PAINTING("Painting"),
    INSULATION("Insulation"),
    FRAMING("Framing"),
    PLUMBING("Plumbing"),
    ELECTRONIC("Electronic");

    private String repairType;

    RepairTypeEnum(String repairType) {
        this.repairType = repairType;
    }

    public String getRepairType() {
        return repairType;
    }

    @Override
    public String toString() {
        return repairType;
    }
}
