package com.codehub.academy.springbootteam1.enums;

public enum PropertyTypeEnum {

    DEFAULT("Undefined"),
    DETACHED("Detached"),
    MAISONETTE("Maisonette"),
    APARTMENT("Apartment");

    private String propertyType;

    PropertyTypeEnum(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyType() {
        return propertyType;
    }

    @Override
    public String toString() {
        return propertyType;
    }
}



