package com.codehub.academy.springbootteam1.enums;

public enum RoleTypeEnum {

    USER("usr"),
    ADMIN("admin");

    private String roleType;

    RoleTypeEnum(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleType() {
        return roleType;
    }

    @Override
    public String toString() {
        return roleType;
    }
}
