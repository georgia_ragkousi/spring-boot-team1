package com.codehub.academy.springbootteam1.enums;

public enum  RepairStatusEnum {
    DEFAULT("Undefined"),
    PENDING("Pending"),
    PROGRESS("Progress"),
    COMPLETED("Completed");
    private String repairStatus;
    RepairStatusEnum (String repairStatus){
        this.repairStatus = repairStatus;
    }

    public String getRepairStatus() {
        return repairStatus;
    }

    @Override
    public String toString() {
        return repairStatus;
    }
}


