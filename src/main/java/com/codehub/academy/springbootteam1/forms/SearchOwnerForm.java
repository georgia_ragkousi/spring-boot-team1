package com.codehub.academy.springbootteam1.forms;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SearchOwnerForm {

    private static final String VAT_PATTERN = "^[0-9]";

    private static final String EMAIL_PATTERN = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\\\.[A-Za-z]{1,63}$";

    private static final int VAT_SIZE = 9;

    @Pattern(regexp = VAT_PATTERN,message = "Invalid VAT Number")
    @Size(min = VAT_SIZE, max = VAT_SIZE, message = "Invalid VAT length")
    private String vat;

    @Pattern(regexp = EMAIL_PATTERN, message = "Invalid Email")
    private String email;

    private String firstName;

    private String lastName;

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
