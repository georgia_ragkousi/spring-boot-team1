package com.codehub.academy.springbootteam1.model;

public class RepairModel {


//    private String id;
    private String date;
    private String status;
    private String type;
    private String cost;
    private String address;
    private String description;
    private String ownerId;

    public RepairModel(String date,
                       String status,
                       String type,
                       String cost,
                       String address,
                       String description,
                       String ownerId) {
        this.date = date;
        this.status = status;
        this.type = type;
        this.cost = cost;
        this.address = address;
        this.description = description;
        this.ownerId = ownerId;
    }

    public RepairModel() {

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
