package com.codehub.academy.springbootteam1.controller;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.model.LoginResponse;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private RepairService service;

    @GetMapping(value = "/user/home")
    public String userRepairs(Model model) {
        SecurityContext contextHolder = SecurityContextHolder.getContext();
        LoginResponse loginResponse = (LoginResponse) contextHolder.getAuthentication().getPrincipal();

        PropertyOwner propertyOwner = loginResponse.getPropertyOwner();
        List<RepairModel> ownerRepairs = service.findByPropertyOwnerId(propertyOwner.getVat());

        model.addAttribute("repairs", ownerRepairs);
        model.addAttribute("userFirstName", propertyOwner.getFirstName());
        model.addAttribute("role", propertyOwner.getRole().name());
        return "pages/repairs_of_day";
    }

}
