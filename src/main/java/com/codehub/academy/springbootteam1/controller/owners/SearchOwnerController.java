package com.codehub.academy.springbootteam1.controller.owners;

import com.codehub.academy.springbootteam1.forms.SearchOwnerForm;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import com.codehub.academy.springbootteam1.service.PropertyOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class SearchOwnerController {

    private static final String OWNERS_LIST = "owners";

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @GetMapping(value = "/searchOwner")
    public String showOwners(Model model){
        List<PropertyOwnerModel> owners = propertyOwnerService.findAll();
        model.addAttribute(OWNERS_LIST, owners);
        return "pages/searchOwner";
    }

    //cannot use /searchOwner here because of ambiguous mapping!!!
//    @GetMapping("/searchForm")
//    public String searchTest(ModelMap modelMap) {
//        modelMap.addAttribute("searchOwnerForm", new SearchOwnerForm());
//        return "pages/searchOwnerForm";
//    }

    @PostMapping("/searchForm/vat")
    public String showOwnerByVat(@ModelAttribute("searchOwnerForm") SearchOwnerForm searchOwnerForm,
                             Model model) {
        PropertyOwnerModel propertyOwnerModel = propertyOwnerService.findByVat(Long.valueOf(searchOwnerForm.getVat()));

        model.addAttribute("VatOwner", propertyOwnerModel);
        return "pages/searchOwner";
    }

    @PostMapping("/searchForm/email")
    public String showOwnerByEmail(@ModelAttribute("searchOwnerForm") SearchOwnerForm searchOwnerForm,
                             Model model) {
        PropertyOwnerModel propertyOwnerModel = propertyOwnerService.findByEmail(searchOwnerForm.getEmail());

        model.addAttribute("VatOwner", propertyOwnerModel);
        return "pages/searchOwner";
    }


}


