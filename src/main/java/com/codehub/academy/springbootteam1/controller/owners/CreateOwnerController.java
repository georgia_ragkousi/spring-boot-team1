package com.codehub.academy.springbootteam1.controller.owners;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CreateOwnerController {

    @GetMapping({"/createOwner"})
    public String first(Model model, @RequestParam(value = "name", required = false, defaultValue = "World") String name) {
        model.addAttribute("name", name);
        return "pages/createOwner";
    }

}
