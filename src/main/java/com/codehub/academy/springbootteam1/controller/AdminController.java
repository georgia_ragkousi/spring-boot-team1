package com.codehub.academy.springbootteam1.controller;

import com.codehub.academy.springbootteam1.enums.RoleTypeEnum;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class AdminController {

    private static final String REPAIRS_LIST = "repairs";

    @Autowired
    private RepairService repairServiceService;

    @GetMapping(value = "/home")
    public String repairsOfDay(Model model){
        List<RepairModel> repairs = repairServiceService.findAll();
        model.addAttribute(REPAIRS_LIST, repairs);
        model.addAttribute("role", RoleTypeEnum.ADMIN.name());
        return "pages/repairs_of_day";
    }
}
