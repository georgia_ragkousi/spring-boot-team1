package com.codehub.academy.springbootteam1.controller.repairs;

import com.codehub.academy.springbootteam1.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
@Controller
public class EditRepairController {

    @Autowired
    private RepairService repairService;

    @GetMapping({"/editRepair"})
    public String sixth(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        return "./pages/editRepair";
    }

    @PostMapping(value = "/repair/{id}/delete")
    public String deleteBook(@PathVariable Long id) {
        repairService.deleteById(id);
        return "redirect:/editRepair";
    }




}
