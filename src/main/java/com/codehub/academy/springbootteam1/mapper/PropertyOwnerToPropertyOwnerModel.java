package com.codehub.academy.springbootteam1.mapper;


import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import org.springframework.stereotype.Component;

@Component
public class PropertyOwnerToPropertyOwnerModel {

    public PropertyOwnerModel mapToPropertyOwnerModel(PropertyOwner owner){

        PropertyOwnerModel ownerModel= new PropertyOwnerModel();

//        ownerModel.setAddress(owner.getAddress());
        ownerModel.setEmail(owner.getEmail());
        ownerModel.setFirstName(owner.getFirstName());
        ownerModel.setLastName(owner.getLastName());
//        ownerModel.setPhoneNumber(String.valueOf(owner.getPhoneNumber()));
//        ownerModel.setPropertyType(owner.getPropertyType().toString());
//        ownerModel.setPassword(owner.getPassword());
//        ownerModel.setRole(owner.getRole().toString());
        ownerModel.setVat(String.valueOf(owner.getVat()));

        return ownerModel;
    }


}
