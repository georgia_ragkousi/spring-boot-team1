package com.codehub.academy.springbootteam1.mapper;

import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.model.RepairModel;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

@Component
public class RepairToRepairModel {

    public RepairModel mapToRepairModel(Repair repair){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        RepairModel repairModel = new RepairModel();

        repairModel.setDate(dateTimeFormatter.format(repair.getRepairDate()));
        repairModel.setStatus(repair.getRepairStatus().toString());
        repairModel.setType(repair.getRepairType().toString());
        repairModel.setCost(String.valueOf(repair.getRepairCost()));
        repairModel.setAddress(repair.getRepairAddress());
        repairModel.setDescription(repair.getDescription());
        repairModel.setOwnerId(String.valueOf(repair.getPropertyOwner().getVat()));

        return repairModel;
    }

}


//@Component
//public class BookToBookModelMapper {
//
//    public BookModel mapToBookModel(Book book) {
//        BookModel bookModel = new BookModel();
//        bookModel.setTitle(book.getTitle());
//        bookModel.setAuthor(book.getAuthor().getFirstName() + " " + book.getAuthor().getLastName());
//        bookModel.setYear(book.getYear());
//        bookModel.setId(book.getId());
//        return bookModel;
//    }
//
//
//}