package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.model.RepairModel;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RepairService {

    //    SEARCH by Date (Equals, After, Between) and by Owner Id functionality

    List<Repair> findByRepairDateAfter(LocalDate repairDate);

    List<Repair> findByRepairDateEquals(LocalDate repairDate);

    List<Repair> findByRepairDateBetween(LocalDate startDate, LocalDate endDate);

    List<RepairModel> findByPropertyOwnerId(Long id);


    List<RepairModel> findAll();

    // DELETE by Id

    void deleteById(Long id);



    //Repair updateRepair(Repair repair);

}