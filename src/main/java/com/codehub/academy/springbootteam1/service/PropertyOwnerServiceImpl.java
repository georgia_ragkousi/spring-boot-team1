package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.mapper.PropertyOwnerToPropertyOwnerModel;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import com.codehub.academy.springbootteam1.repository.PropertyOwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class PropertyOwnerServiceImpl implements PropertyOwnerService {

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @Autowired
    private PropertyOwnerToPropertyOwnerModel mapper;

//    SEARCH by Vat and Email functionality

    @Override
    public PropertyOwnerModel findByVat(Long vat) {
        PropertyOwner propertyOwner = propertyOwnerRepository.findById(vat).get();
        return mapper.mapToPropertyOwnerModel(propertyOwner);
    }

    @Override
    public PropertyOwnerModel findByEmail(String email) {
        PropertyOwner propertyOwner = propertyOwnerRepository.findByEmail(email).get();
        return mapper.mapToPropertyOwnerModel(propertyOwner);
    }

    @Override
    public List<PropertyOwnerModel> findAll() {
        return propertyOwnerRepository
                .findAll()
                .stream()
                .map(owner -> mapper.mapToPropertyOwnerModel(owner))
                .collect(Collectors.toList());
//        List<PropertyOwner> propertyOwners = propertyOwnerRepository.findAll();
//
//        List<PropertyOwnerModel> propertyOwnerModels = new LinkedList<>();
//
//        for (PropertyOwner propertyOwner : propertyOwners) {
//            PropertyOwnerModel propertyOwnerModel = mapper.mapToPropertyOwnerModel(propertyOwner);
//            propertyOwnerModels.add(propertyOwnerModel);
//        }
//        return propertyOwnerModels;
    }

    @Override
    public void deleteByVat(Long id) {
        propertyOwnerRepository.deleteById(id);

    }


//    @Override
//    public Optional<PropertyOwner> createPropertyOwner(PropertyOwner propertyOwner) {
//        return propertyOwnerRepository.save(propertyOwner);
//    }
}
