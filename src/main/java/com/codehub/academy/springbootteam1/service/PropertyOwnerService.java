package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;

import java.util.List;
import java.util.Optional;

public interface PropertyOwnerService {

    //SEARCH

    PropertyOwnerModel findByVat(Long vat);

    PropertyOwnerModel findByEmail(String email);

    List<PropertyOwnerModel> findAll();

    void deleteByVat(Long id);

//    Optional<PropertyOwner> createPropertyOwner(PropertyOwner propertyOwner);

//    DELETE is redundant since its by JPA default ready

//    void deletePropertyOwner(Long id);

}
