package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.mapper.RepairToRepairModel;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    private RepairRepository repairRepository;

    @Autowired
    private RepairToRepairModel mapper;

    //    SEARCH by Date (Equals, After & Between) and by Owner Id functionality

    @Override
    public List<Repair> findByRepairDateAfter(LocalDate repairDate) {
        return repairRepository.findByRepairDateIsAfter(repairDate);
    }

    @Override
    public List<Repair> findByRepairDateEquals(LocalDate repairDate) {
        return repairRepository.findByRepairDate(repairDate);
    }

    @Override
    public List<Repair> findByRepairDateBetween(LocalDate startDate, LocalDate endDate) {
        return repairRepository.findByRepairDateBetween(startDate, endDate);
    }

    @Override
    public List<RepairModel> findByPropertyOwnerId(Long id) {
        List<RepairModel> repairModels = new ArrayList<>();

        List<Repair> repairList = repairRepository.findByPropertyOwnerId(id);

        for (Repair repair : repairList) {
            RepairModel repairModel = mapper.mapToRepairModel(repair);
            repairModels.add(repairModel);
        }
        return repairModels;
    }

    @Override
    public List<RepairModel> findAll() {
        return repairRepository
                .findAll()
                .stream()
                .map(repair -> mapper.mapToRepairModel(repair))
                .collect(Collectors.toList());
    }

    // DELETE

    @Override
    public void deleteById(Long id) {
        repairRepository.deleteById(id);
    }


}
