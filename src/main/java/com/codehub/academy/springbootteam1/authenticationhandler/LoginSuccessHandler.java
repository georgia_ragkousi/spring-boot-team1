package com.codehub.academy.springbootteam1.authenticationhandler;

import com.codehub.academy.springbootteam1.enums.RoleTypeEnum;
import com.codehub.academy.springbootteam1.model.LoginResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

import static com.codehub.academy.springbootteam1.utils.GlobalAttributes.TIMESTAMP_COOKIE_NAME;

@Component
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private static final String USER_HOME_PAGE_URL = "/user/home";
    private static final String REPAIRS_OF_THE_DAY_PAGE_URL = "/home";

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        response.addCookie(generateTimestampCookie());

        //LoginResponse loginResponse = (LoginResponse) authentication.getPrincipal();
        //String redirectUrl = String.format(USER_HOME_PAGE_URL, loginResponse.getUsername());

        String redirectUrl = USER_HOME_PAGE_URL;

        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            if (RoleTypeEnum.ADMIN.name().equals(grantedAuthority.getAuthority())) {
                redirectUrl = REPAIRS_OF_THE_DAY_PAGE_URL;
            }
        }
        redirectStrategy.sendRedirect(request, response, redirectUrl);
    }


    private Cookie generateTimestampCookie() {
        return new Cookie(TIMESTAMP_COOKIE_NAME, String.valueOf(System.currentTimeMillis()));
    }

}
